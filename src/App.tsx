import React from 'react';

import { DemoApp } from './components/demoapp'

function App() {
  return (
    <div className="App">
      <DemoApp title="MyCoolApp" color="#1487b1" />
    </div>
  );
}

export default App;
