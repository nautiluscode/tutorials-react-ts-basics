import React from 'react';



export class DemoApp extends React.Component<{ title: string, color: string }> {
    state = {
        counter: 0
    }

    handleButtonClick = () => {
        this.setState({counter: this.state.counter + 1})
    }

    render() {
        return <div><div style={{
            padding: 20,
            backgroundColor: this.props.color
        }}>{this.props.title}</div>


        <button onClick={this.handleButtonClick}>Something</button>

        <h2>{this.state.counter}</h2>

        </div>
    }
}